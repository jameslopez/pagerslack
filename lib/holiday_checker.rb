# frozen_string_literal: true

require 'date'
require 'yaml'

class HolidayChecker
  HOLIDAYS_FILENAME = './holidays.yml'

  def initialize
    @holidays = YAML.load(File.read(HOLIDAYS_FILENAME))
  end

  def today?
    weekend? || holiday?
  end

  private

  def weekend?
    Time.now.utc.saturday? || Time.now.utc.sunday?
  end

  def holiday?
    @holidays.any?(Time.now.utc.to_date)
  end
end
