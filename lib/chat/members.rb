# frozen_string_literal: true

require './lib/chat/client'
require './lib/chat/incident_summary'
require './lib/chat/follow_up_poster'
require './lib/store'
require './lib/config'
require 'active_support/core_ext/integer/inflections'
require 'csv'

module Chat
  class Members
    include SemanticLogger::Loggable

    NotRespondingMemberError = Class.new(StandardError)

    DEFAULT_MEMBER_SCORE = 0
    WAIT_TIME_BUFFER_IN_MINUTES = 1

    def self.remove_member(member)
      Store.zrem(:members, member)
    end

    def initialize(event = nil, issue_url = nil, user = nil)
      @event = event
      @issue_url = issue_url
      @user = user
      @ping_attempts = 0
      @pinged_members = []
      @ack_message_ts = nil
    end

    def respond_to_escalation
      if Store.get(:active_ping)
        reply_to_escalating_user(user: @user, issue_link: issue_link, succeeded: false)
        logger.info('Escalation rejected', payload: { issue_url: @issue_url, active_ping_ts: Store.get(:active_ping) })
      else
        logger.info('New incident', issue_url: @issue_url, reported_by: client.get_username(@user))
        notify_online_member
      end
    end

    def notify_online_member
      @event.reset

      message = reply_to_escalating_user(user: @user, issue_link: issue_link, succeeded: true)
      @ack_message_ts = message.ts
      Store.set(:active_ping, @ack_message_ts)

      logger.measure_info('Time to response', payload: { issue_url: @issue_url }, metric: 'total/ack_time') do
        find_and_notify
      end

      Store.del(:active_ping)

      if @event.set?
        IncidentSummary.new(reporter: @user, pinged_members: pinged_member_names, ack_member: ack_member, ts: @ack_message_ts).async.post
        FollowUpPoster.new(@pinged_members - [last_pinged]).async.post
      else
        no_member_found
      end

      @event.reset
    end

    def least_frequent
      return cached_members unless members_expired?

      member_args = ([DEFAULT_MEMBER_SCORE.to_f] * oncall_members.count).zip(oncall_members)

      # Let's make sure when we update the members, the score doesn't get updated
      Store.zadd(:members, member_args, { nx: true, xx: false })

      refresh_members
      cached_members
    end

    private

    def reply_to_escalating_user(user:, issue_link:, succeeded:)
      if succeeded
        user_output = user == Config.gitlab_bot ? "<##{Config.master_broken_channel}>" : "<@#{user}>"
        client.message("#{user_output} thanks for reporting a #{issue_link}. Bear with me while I find someone to help.")
      else
        message = "Escalation rejected for #{issue_link} due to another active incident in <##{Config.channel_id}>. Please retry in #{minutes_before_retry} minutes."
        message_receiver = user == Config.gitlab_bot ? Config.master_broken_channel : user
        client.pm_message(message, user: message_receiver)
      end
    end

    def minutes_before_retry
      response_wait_time - (Time.now.to_i - Store.get(:active_ping).to_i) / 60 + WAIT_TIME_BUFFER_IN_MINUTES
    end

    def find_and_notify(weekend_engineers: false)
      logger.warn('No weekday engineers found, trying with weekend engineers', issue_url: @issue_url) if weekend_engineers

      least_frequent.each do |member|
        break if @event.set? || exceeded_pings?

        next unless oncall?(member, weekend_engineers: weekend_engineers)
        next unless client.online?(member)

        send_message(member)
        @ping_attempts += 1
      end

      find_and_notify(weekend_engineers: true) unless skip_weekend?(weekend_engineers)
    end

    def skip_weekend?(weekend_engineers)
      exceeded_pings? || weekend_engineers || @event.set?
    end

    def exceeded_pings?
      @ping_attempts >= Config.max_pings
    end

    def no_member_found
      logger.warn('user unavailable', issue_url: @issue_url, response_wait_time: response_wait_time)

      message = client.message("<!subteam^#{Config.escalation_group_id}> unfortunately nobody is available. Please help us find an engineer on the thread above. To avoid duplicating efforts let us know you are finding someone and who. Thanks!")

      IncidentSummary.new(reporter: @user, pinged_members: pinged_member_names, ack_member: nil, ts: message.ts).async.post
      FollowUpPoster.new(@pinged_members).async.post
    end

    def response_wait_time
      Config.max_pings * Config.next_ping_wait / 60
    end

    def send_message(member)
      return if @event.set?
      return unless Store.get(:active_ping)

      client.update_message(ping_message(member), ts: @ack_message_ts)
      client.message(thread_ping_message(member), ts: @ack_message_ts)

      @pinged_members << member

      logger.measure_info('User pinged', payload: { username: username(member), issue_url: @issue_url }, metric: 'user/ack_time') do
        @event.wait(Config.next_ping_wait)

        raise NotRespondingMemberError unless @event.set?
      end
    rescue NotRespondingMemberError
      # Do nothing
    end

    def username(member)
      oncall_list[member]&.first&.dig('name')
    end

    def ping_message(member)
      ":warning: (#{(@ping_attempts + 1).ordinalize} ping) <@#{member}> a #{issue_link} has been reported! Please react *to this message* with :#{Config.positive_reaction}: to acknowledge. Any engineer can also react."
    end

    def thread_ping_message(member)
      ":bell: Ping #{(@ping_attempts + 1)}: <@#{member}>"
    end

    def issue_link
      "<#{@issue_url}|new incident>"
    end

    def cached_members
      # Retrieve up to Config.max_member_count members, in ascending order of score (timestamp)
      Store.zrangebyscore(:members, 0, '+Inf', limit: [0, Config.max_member_count])
    end

    def members_expired?
      !Store.get(:members_cached)
    end

    def refresh_members
      Store.set(:members_cached, 1, ex: Config.members_expire_time)
    end

    def ack_member
      username(last_pinged) if last_pinged
    end

    def last_pinged
      Store.zrevrangebyscore(:members, '+Inf', 1, limit: [0, 1])&.first
    end

    def pinged_member_names
      @pinged_members.map { |member| username(member) }
    end

    def client
      Chat::Client.instance
    end

    def oncall?(member, weekend_engineers: false)
      return false unless oncall_list[member]

      weekend_allowed = oncall_list[member].flatten.last == 'Yes'

      weekend_engineers == weekend_allowed
    end

    def oncall_list
      @oncall_list ||= CSV.read('./oncall.csv', headers: true).group_by { |csv| csv[1] }
    end

    def oncall_members
      @oncall_members ||= client.members.find_all { |member| oncall_list[member] }
    end
  end
end
