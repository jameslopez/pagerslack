# frozen_string_literal: true

require './lib/store'
require './lib/chat/client'
require './lib/chat/members'

module Chat
  class PingForecaster
    include SemanticLogger::Loggable

    TOP_MEMBER_COUNT = 25

    def initialize(user)
      @user = user
    end

    def position
      pos = rank

      logger.debug('private message: position', user: client.get_username(@user))

      if pos
        client.pm_message("you are at position #{pos}/#{oncall_total} in the queue", user: @user)
      else
        client.pm_message("<@#{@user}> Sorry, I can't seem to find you in the oncall list", user: @user)
      end
    end

    def top
      logger.debug('private message: top', user: client.get_username(@user))

      refresh_member_list # in case this hasn't been loaded

      client.pm_message("The top members in the queue are: ```#{top_weekday.join("\n")}```", user: @user)
    end

    private

    def refresh_member_list
      Chat::Members.new.least_frequent
    end

    # TODO: DRY this
    def oncall_list
      @oncall_list ||= CSV.read('./oncall.csv', headers: true).group_by { |csv| csv[1] }
    end

    def oncall_total
      oncall_list.count
    end

    def rank
      return nil unless oncall_list[@user]

      refresh_member_list # in case this hasn't been loaded

      if weekday_oncall?(@user)
        rank_weekdays
      else
        rank_weekend
      end
    end

    def rank_weekend
      index = 0

      Store.zrange(:members).each do |ranked_member|
        next unless oncall_list[ranked_member]
        next if weekday_oncall?(ranked_member)

        index += 1

        break if @user == ranked_member
      end

      index + weekday_total unless index.zero?
    end

    def weekday_total
      oncall_list.count { |o| o.flatten.last != 'Yes' }
    end

    def rank_weekdays
      index = 0

      Store.zrange(:members).each do |ranked_member|
        next unless oncall_list[ranked_member]
        next unless weekday_oncall?(ranked_member)

        index += 1

        return index if @user == ranked_member
      end

      nil
    end

    def top_weekday
      Store.zrange(:members).each_with_object([]) do |ranked_member, top|
        next unless oncall_list[ranked_member]
        next unless weekday_oncall?(ranked_member)

        top << "#{top.count + 1}. #{oncall_list[ranked_member].first['name']}"

        return top if top.count == TOP_MEMBER_COUNT
      end
    end

    def weekday_oncall?(member)
      oncall_list[member].flatten.last != 'Yes'
    end

    def client
      Chat::Client.instance
    end
  end
end
