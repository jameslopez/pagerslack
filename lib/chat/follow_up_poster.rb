# frozen_string_literal: true

require './lib/chat/client'

module Chat
  class FollowUpPoster
    include Concurrent::Async
    include SemanticLogger::Loggable

    def initialize(members)
      super()
      @members = members
    end

    def post
      @members.each do |member|
        logger.debug('Following up with no responder', member: member)
        client.pm_message(message(member), user: member)
      end
    end

    private

    def message(member)
      "Hey <@#{member}>, I noticed you did not respond to the incident in <##{Config.channel_id}>."\
       " Please note responding to an incident is of <https://about.gitlab.com/handbook/on-call/#expectations-for-on-call|higher priority> than meetings or other work unless you are working on a S1."\
       " Please remember to update your <https://handbook.gitlab.com/handbook/engineering/development/processes/infra-dev-escalation/process/#notification-settings|Slack working hours and notification settings>"\
       " and know that you can use `top` and `position` here to check your position in the queue."
    end

    def client
      Chat::Client.instance
    end
  end
end
