# frozen_string_literal: true

require 'uri'
require './lib/config'
require './lib/holiday_checker'
require './lib/chat/members'
require './lib/chat/ping_forecaster'

module Chat
  class MessageParser
    include SemanticLogger::Loggable

    SPREADSHEET_URL = 'https://tinyurl.com/wtdao82'

    def initialize(event, data)
      @event = event
      @data = data
      @message = data['text'] || ''
      @user = data['user'] || data['user_id'] || data['username']
      @ts = data['ts']
      @command = data['command']
    end

    def parse
      if @message == 'position'
        PingForecaster.new(@user).position
      elsif @message == 'top'
        PingForecaster.new(@user).top
      elsif @message == 'test-reaction' && !Config.slack_output
        Thread.new { Chat::ReactionResponder.new(@event, @data).respond }
      elsif HolidayChecker.new.today?
        respond_to_holiday unless bot_escalated_incident?
      elsif @command == Config.command || bot_escalated_incident?
        issue_url ? respond_to_incident : invalid_incident
      end
    end

    private

    def respond_to_holiday
      logger.info('Weekend/holiday triggered - ignoring', reported_by: client.get_username(@user))

      client.message("Please use the spreadsheet for weekend or holiday escalations: #{SPREADSHEET_URL}")
    end

    def bot_escalated_incident?
      @message.start_with?(Config.command.delete('/')) && @user == Config.gitlab_bot
    end

    def respond_to_incident
      client.pm_message('Sorry, @pagerslack is currently in maintenance mode. Please wait a few seconds and try again.', user: @user, force: true) unless Config.slack_output

      Chat::Members.new(@event, issue_url, @user).respond_to_escalation
    end

    def invalid_incident
      logger.debug('Invalid incident (no URL)', reported_by: client.get_username(@user), message: @message)

      client.pm_message("<@#{@user}> please pass an incident URL: `#{Config.command} https://incident.url`", user: @user)
    end

    def issue_url
      @issue_url ||= URI.extract(@message)&.first
    end

    def client
      Chat::Client.instance
    end
  end
end
