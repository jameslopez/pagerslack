# frozen_string_literal: true

require 'spec_helper'
require 'slack'
require 'semantic_logger'
require './lib/config'

describe Chat::Client do
  let(:slack_client_double) { instance_double(Slack::Web::Client) }

  subject(:chat_client) { described_class.instance }

  before do
    allow(chat_client).to receive(:client).and_return(slack_client_double)
  end

  describe '#message' do
    context 'without passing timestamp' do
      it 'sends message to channel ID, without thread_ts' do
        if Config.slack_output
          expect(slack_client_double).to receive(:chat_postMessage).with(
            channel: Config.channel_id,
            text: 'message',
            as_user: true,
            thread_ts: nil
          )
        else
          expect_any_instance_of(SemanticLogger::Logger).to receive(:debug).with(
            payload: { message: 'message', message_in_thread: nil }
          )
        end

        chat_client.message('message')
      end
    end

    context 'with timestamp' do
      let(:ts) { Time.new(2024).to_f }

      it 'sends message to channel ID and thread_ts as a reply message' do
        if Config.slack_output
          expect(slack_client_double).to receive(:chat_postMessage).with(
            channel: Config.channel_id,
            text: 'message',
            as_user: true,
            thread_ts: ts
          )
        else
          expect_any_instance_of(SemanticLogger::Logger).to receive(:debug).with(
            payload: { message: 'message', message_in_thread: ts }
          )
        end

        chat_client.message('message', ts: ts)
      end
    end
  end

  describe '#pm_message' do
    let(:message) { 'message' }
    let(:user)    { 'user' }

    context 'with user not equal to gitlab-bot' do
      it 'calls Slack::Web::Client instance with chat_postMessage' do
        if Config.slack_output
          expect(slack_client_double).to receive(:chat_postMessage).with(
            channel: user,
            text: message,
            as_user: true
          )
        else
          expect_any_instance_of(SemanticLogger::Logger).to receive(:debug).with(
            payload: { message: message, message_in_thread: false, to_user: user }
          )
        end

        chat_client.pm_message(message, user: user, force: false)
      end
    end

    context 'with user equal to gitlab-bot' do
      let(:user) { 'gitlab-bot' }

      it 'adds logging without calling Slack' do
        expect(slack_client_double).not_to receive(:chat_postMessage)
        expect_any_instance_of(SemanticLogger::Logger).to receive(:debug).with(
          payload: { message: message, message_in_thread: false, to_user: user }
        )

        chat_client.pm_message(message, user: user, force: false)
      end
    end
  end
end
