# frozen_string_literal: true

require 'spec_helper'
require 'semantic_logger'
require 'slack'
require './lib/config'
require './lib/holiday_checker'
require './lib/chat/ping_forecaster'
require './lib/chat/message_parser'

describe Chat::MessageParser do
  include_context 'with stubbed Store'

  describe '#parse' do
    let(:event)        { double('event').as_null_object }
    let(:client)       { double('client').as_null_object }
    let(:event_data)   { { 'text' => 'position', 'user' => 'user' } }

    subject(:message_parser) { described_class.new(event, event_data) }

    before do
      allow_any_instance_of(Chat::PingForecaster).to receive(:oncall_list).with(any_args).and_return({ 'member' => [{ 'name': 'member', 'slack': 'member', weekend: 'No' }] })
      allow_any_instance_of(HolidayChecker).to receive(:today?).and_return(false)
      allow_any_instance_of(described_class).to receive(:client).and_return(client)
      allow_any_instance_of(Chat::Members).to receive(:client).and_return(client)
      allow_any_instance_of(Chat::Members).to receive(:oncall_list).and_return({ 'member' => [{ 'name': 'member' }] })

      stub_const('HolidayChecker::HOLIDAYS_FILENAME', './holidays.yml.example')
    end

    it 'works' do
      expect(message_parser.parse).not_to be_nil
    end

    context 'with an incident' do
      context 'with /devoncall command' do
        context 'with url' do
          let(:event_data) { { 'text' => 'https://url', 'user' => 'member', 'command' => Config.command } }

          it 'works' do
            expect(client).to receive(:message).with(
              '<@member> thanks for reporting a <https://url|new incident>. Bear with me while I find someone to help.'
            )

            message_parser.parse
          end
        end

        context 'without a URL' do
          let(:event_data) { { 'user' => 'member', 'command' => Config.command } }

          it 'does not work and sends a private message to user' do
            expect(client).to receive(:pm_message).with(
              '<@member> please pass an incident URL: `/devoncall https://incident.url`', user: 'member'
            )

            message_parser.parse
          end
        end

        context 'when there is another active ping being processed' do
          let(:event_data) { { 'text' => 'https://url', 'user' => 'member', 'command' => Config.command } }
          let(:minutes_since_active_ping) { 5 }

          before do
            allow(store_double).to receive(:get).with(:active_ping).and_return(minutes_since_active_ping.minutes.ago)
          end

          it 'replies to escalating user with a rejection message' do
            minutes_before_retry = 12 - minutes_since_active_ping + Chat::Members::WAIT_TIME_BUFFER_IN_MINUTES
            expect(client).to receive(:pm_message).with(
              "Escalation rejected for <https://url|new incident> due to another active incident in <##{Config.channel_id}>. Please retry in #{minutes_before_retry} minutes.",
              user: 'member'
            )

            message_parser.parse
          end
        end
      end

      context 'with devoncall message' do
        let(:event_data_base) { { 'text' => 'devoncall https://url' } }

        context 'with human user' do
          let(:event_data) { event_data_base.merge({ 'user' => 'human' }) }

          it 'triggers no message' do
            expect(client).not_to receive(:pm_message)
            expect(client).not_to receive(:message)

            message_parser.parse
          end
        end

        context 'with a random bot user and url' do
          let(:event_data) { event_data_base.merge({ 'username' => 'bot', 'bot_id' => 'bot_id' }) }

          it 'triggers no message' do
            expect(client).not_to receive(:message)
            expect(client).not_to receive(:pm_message)

            message_parser.parse
          end
        end

        context 'with gitlab-bot user without a URL' do
          let(:event_data) { { 'text' => 'devoncall', 'username' => 'gitlab-bot', 'bot_id' => 'bot_id' } }

          it 'does not work and triggers a pm_message call' do
            expect(client).to receive(:pm_message).with(
              '<@gitlab-bot> please pass an incident URL: `/devoncall https://incident.url`', user: 'gitlab-bot'
            )

            message_parser.parse
          end
        end

        context 'with gitlab-bot user and url' do
          let(:event_data) { event_data_base.merge({ 'username' => 'gitlab-bot', 'bot_id' => 'bot_id' }) }

          it 'works' do
            expect(client).to receive(:message).with(
              "<##{Config.master_broken_channel}> thanks for reporting a <https://url|new incident>. Bear with me while I find someone to help."
            )

            message_parser.parse
          end
        end

        context 'with gitlab-bot user and url, while there is another active ping being processed' do
          let(:event_data) { event_data_base.merge({ 'username' => 'gitlab-bot', 'bot_id' => 'bot_id' }) }
          let(:minutes_since_active_ping) { 5 }

          before do
            allow(store_double).to receive(:get).with(:active_ping).and_return(minutes_since_active_ping.minutes.ago)
          end

          it 'replies to master-broken channel with a rejection message' do
            minutes_before_retry = 12 - minutes_since_active_ping + Chat::Members::WAIT_TIME_BUFFER_IN_MINUTES
            expect(client).to receive(:pm_message).with(
              "Escalation rejected for <https://url|new incident> due to another active incident in <#CLKLMSUR4>. Please retry in #{minutes_before_retry} minutes.",
              user: Config.master_broken_channel
            )

            message_parser.parse
          end
        end
      end
    end

    context 'when escalated on a holiday' do
      let(:event_data_base) { { 'text' => 'devoncall https://url' } }

      before do
        allow_any_instance_of(HolidayChecker).to receive(:today?).and_return(true)
      end

      context 'with gitlab-bot user' do
        let(:event_data) { event_data_base.merge({ 'username' => 'gitlab-bot', 'bot_id' => 'bot_id' }) }

        it 'does not trigger any escalation or message' do
          expect(client).not_to receive(:message)

          message_parser.parse
        end
      end

      context 'with human user' do
        let(:event_data) { event_data_base.merge({ 'user' => 'human' }) }

        it 'responds with instructions on how to escalate on holiday' do
          expect(client).to receive(:message).with(
            'Please use the spreadsheet for weekend or holiday escalations: https://tinyurl.com/wtdao82'
          )

          message_parser.parse
        end
      end
    end
  end
end
