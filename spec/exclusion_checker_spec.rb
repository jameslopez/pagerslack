# frozen_string_literal: true

require 'spec_helper'
require 'semantic_logger'

require './lib/exclusion_checker'

describe ExclusionChecker do
  before do
    stub_const('ExclusionChecker::EXCLUSIONS_FILENAME', './exclusions.yml.example')
  end

  subject(:exclusion) { described_class.new }

  it 'returns true for someone in the list' do
    expect(subject.excluded?('name@example.com')).to be true
  end

  it 'returns false for someone not in the list' do
    expect(subject.excluded?('name-2@example.com')).to be false
  end
end
