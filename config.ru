# frozen_string_literal: true

require 'semantic_logger'
require './lib/auth'
require './lib/api'

# Initialize the app and create the API (bot) and Auth objects.
run Rack::Cascade.new [API, Auth]
